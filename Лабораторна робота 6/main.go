package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type Variables struct {
	Title   string
	Heading string
	IsPost  bool
	FirstName string
	LastName string
	Age string
	Phone string
}

const tmplFile1 = "main1.tpl"
const tmplFile2 = "main2.tpl"
const tmplFile3 = "main3.tpl"
const tmplFile4 = "main4.tpl"
const tmplFile5 = "main5.tpl"
const (
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	myVars := Variables{}

	tmpl, err := template.ParseFiles(tmplFile1)
	templates1 := template.Must(tmpl, err)
	tmpl2, err := template.ParseFiles(tmplFile2)
	templates2 := template.Must(tmpl2, err)
	tmpl3, err := template.ParseFiles(tmplFile3)
	templates3 := template.Must(tmpl3, err)
	tmpl4, err := template.ParseFiles(tmplFile4)
	templates4 := template.Must(tmpl4, err)
	tmpl5, err := template.ParseFiles(tmplFile5)
	templates5 := template.Must(tmpl5, err)

	myVars.Title = "Page1"
	myVars.Heading = "Hi!"
	myVars.IsPost = false

	if r.Method == "POST" { // Обрабатываем входные данные
		myVars.IsPost = true
		err = r.ParseForm() // Парсим форму
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		myVars.FirstName = post.Get("firstName")
		myVars.LastName = post.Get("lastName")
		myVars.Age = post.Get("age")
		myVars.Phone = post.Get("phone")
	}

	if r.URL.Path == "/" {
		templates1.ExecuteTemplate(w, tmplFile1, myVars)
	}

	if r.URL.Path == "/2" { 
		myVars.Title = "Page2"
		myVars.Heading = "Hi!"
		myVars.FirstName = "Богдана"
		myVars.LastName = "Сак"
		myVars.Age = "20"
		myVars.Phone = "222222"
		templates2.ExecuteTemplate(w, tmplFile2, myVars)
	}

	if r.URL.Path == "/3" { 
		myVars.Title = "Page3"
		myVars.Heading = "Добрий день!"
		myVars.FirstName = "Богдана"
		myVars.LastName = "Сак"
		myVars.Age = "20"
		myVars.Phone = "333333"
		templates3.ExecuteTemplate(w, tmplFile3, myVars)
	}

	if r.URL.Path == "/4" { 
		myVars.Title = "Page4"
		myVars.Heading = "Добрий день!"
		myVars.FirstName = "test"
		myVars.LastName = "test"
		myVars.Age = "23"
		myVars.Phone = "444444"
		templates4.ExecuteTemplate(w, tmplFile4, myVars)
	}

	if r.URL.Path == "/5" { 
		myVars.Title = "Page5"
		myVars.Heading = "Добрий день!"
		myVars.FirstName = "test"
		myVars.LastName = "test"
		myVars.Age = "57"
		myVars.Phone = "555555"
		templates5.ExecuteTemplate(w, tmplFile5, myVars)
	}

}
func main() {
	// Запускаем локальный сервер
	http.ListenAndServe("localhost:80", HttpSolution1)
}
