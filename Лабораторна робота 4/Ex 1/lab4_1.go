package main

import (
	"github.com/andlabs/ui"
	"strconv"
)

func initGUI() {
	window := ui.NewWindow("Lab4 1", 500, 600, false)
	label1 := ui.NewLabel("Розміри вікна")
	label2 := ui.NewLabel("Ширина(см)")
	widthInput := ui.NewEntry()
	label3 := ui.NewLabel("Висота(см)")
	heightInput := ui.NewEntry()
	label4 := ui.NewLabel("Матеріал")
	combo1 := ui.NewCombobox()
	combo1.Append("Дерево")
	combo1.Append("Метал")
	combo1.Append("Металопластиковий")
	label5 := ui.NewLabel("Склопакет")
	combo2 := ui.NewCombobox()
	combo2.Append("Однокамерний")
	combo2.Append("Двокамерний")
	checkbox := ui.NewCheckbox("Підвіконня")
	button := ui.NewButton("Розрахувати")
	labelResult := ui.NewLabel("")

	box := ui.NewVerticalBox()
	box.Append(label1, false)
	box.Append(label2, false)
	box.Append(widthInput, false)
	box.Append(label3, false)
	box.Append(heightInput, false)
	box.Append(label4, false)
	box.Append(combo1, false)
	box.Append(label5, false)
	box.Append(combo2, false)
	box.Append(checkbox, false)
	box.Append(button, false)
	box.Append(labelResult, false)
	window.SetChild(box)

	button.OnClicked(func(*ui.Button) {
		width, _ := strconv.ParseFloat(widthInput.Text(), 32)
		height, _ := strconv.ParseFloat(heightInput.Text(), 32)
		result := width * height

		if combo1.Selected() == 1 && combo2.Selected() == 1 {
			result *= 0.25
		}
		if combo1.Selected() == 1 && combo2.Selected() == 2 {
			result *= 0.3
		}
		if combo1.Selected() == 2 && combo2.Selected() == 1 {
			result *= 0.05
		}
		if combo1.Selected() == 2 && combo2.Selected() == 2 {
			result *= 0.1
		}
		if combo1.Selected() == 3 && combo2.Selected() == 1 {
			result *= 0.15
		}
		if combo1.Selected() == 3 && combo2.Selected() == 2 {
			result *= 0.2
		}

		if checkbox.Checked() {
			result += 35
		}

		labelResult.SetText("Результат = " + strconv.FormatFloat(result, 'g', 1, 64))
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
