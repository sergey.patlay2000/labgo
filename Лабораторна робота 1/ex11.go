package main

import "fmt"

func main() {
	fmt.Println("Рівняня виду: ay=bx + c")

	fmt.Println("Ввеідть коефіціенти для першого рівняння")
	var a1 float64 = 3
	var b1 float64 = 2
	var c1 float64 = 2

	fmt.Println("Ввеідть коефіціенти для другого рівняння")
	var a2 float64 = 6
	var b2 float64 = 3
	var c2 float64 = 6

	var z1 float64 = (a2 * c1) - (a1 * c2)
	fmt.Println("Z1 = ", z1)
	var z2 float64 = (a1 * b2) - (a2 * b1)
	fmt.Println("Z2 = ", z2)

	var x float64 = 0

	if z1 == 0 || z2 == 0 {
		x = 0
	} else {
		x = z1 / z2
	}

	var y1 float64 = (b1*x + c1) / a1
	var y2 float64 = (b2*x + c2) / a2

	if y1 != y2 {
		fmt.Println("Роз'язків немає")
	} else {
		fmt.Println("X = ", x)
		fmt.Println("Y = ", y1)
	}

}