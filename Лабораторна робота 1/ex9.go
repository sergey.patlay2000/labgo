package main

import "fmt"

func main() {
	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true

	fmt.Println("first  = ", first)       // false  //Значение по умолчанию
	fmt.Println("second = ", second)      // false  //Значение по умолчанию
	fmt.Println("third  = ", third)       // true   //Значение при инициализации
	fmt.Println("fourth = ", fourth)      // false  //Присвоение инверсного значения
	fmt.Println("fifth  = ", fifth, "\n") // true   //Значение при инициализации

	fmt.Println("!true  = ", !true)        // false  //Инверсия true
	fmt.Println("!false = ", !false, "\n") // true  //Инверсия false

	fmt.Println("true && true   = ", true && true)         // true  //true когда все условия true
	fmt.Println("true && false  = ", true && false)        // false  //true когда все условия true, одно условия false
	fmt.Println("false && false = ", false && false, "\n") // false  //true когда все условия true, два условия false

	fmt.Println("true || true   = ", true || true)         // true  //true когда хотя бы одно условия true
	fmt.Println("true || false  = ", true || false)        // true  //true когда хотя бы одно условия true
	fmt.Println("false || false = ", false || false, "\n") // false //true когда хотя бы одно условия true, все false

	fmt.Println("2 < 3  = ", 2 < 3)        // true  // 2 меньше 3
	fmt.Println("2 > 3  = ", 2 > 3)        // false  // 2 меньше 3
	fmt.Println("3 < 3  = ", 3 < 3)        // false  // 3 не меньше 3
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true // 3 меньше равно 3 (равно)
	fmt.Println("3 > 3  = ", 3 > 3)        // false  // 3 не больше 3
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true  // 3 больше равно 3 (равно)
	fmt.Println("2 == 3 = ", 2 == 3)       // false  // 2 не равно 3
	fmt.Println("3 == 3 = ", 3 == 3)       // true  // 3 равно 3
	fmt.Println("2 != 3 = ", 2 != 3)       // true  // 2 не равно 3
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false  // 3 равно 3

	//Задание.
	//1. Пояснить результаты операций
}
