package main

import (
	"fmt"
	"math"
	"math/rand"
)

func main() {
	m := 15
	a := rand.Intn(m)
	c := rand.Intn(m)
	arr := make([]int, m)
	arr[0] = rand.Intn(m)
	for i := 1; i < m; i++ {
		arr[i] = int(math.Mod(float64(arr[i-1]*a+c), float64(m)))
	}
	fmt.Println(arr)
	fmt.Println("Статистична імовірність")
	statisticalProbability(arr, m)
	fmt.Println("Математичне сподівання")
	mathematicalHopeValue := mathematicalHope(arr, m)
	fmt.Println(mathematicalHopeValue)
	fmt.Println("Дисперсія")
	dispersionValue := dispersion(arr, m, mathematicalHopeValue)
	fmt.Println(dispersionValue)
	fmt.Println("Середньоквадратичне відхилення")
	fmt.Println(math.Sqrt(dispersionValue))
}

func statisticalProbability(arr []int, m int) {
	for i := 0; i < m; i++ {
		count := repeatCheck(arr, m, arr[i])
		fmt.Printf("%d == %f \n", arr[i], float64(count)/float64(m))
		count = 0
	}
}

func mathematicalHope(arr []int, m int) float64 {
	sum := 0.0
	for i := 0; i < m; i++ {
		count := repeatCheck(arr, m, arr[i])
		sum += float64(arr[i]) * float64(count) / float64(m)
		count = 0
	}
	return sum
}

func dispersion(arr []int, m int, mathematicalHopeValue float64) float64 {
	sum := 0.0
	for i := 0; i < m; i++ {
		count := repeatCheck(arr, m, arr[i])
		sum += math.Pow(float64(arr[i])-mathematicalHopeValue, 2) * float64(count) / float64(m)
		count = 0
	}
	return sum
}

func repeatCheck(arr []int, m int, value int) int {
	count := 0
	for j := 0; j < m; j++ {
		if value == arr[j] {
			count++
		}
	}
	return count
}
